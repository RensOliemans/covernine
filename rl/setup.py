from setuptools import setup

from rl.version import VERSION

setup(name='covernine-rl',
      version=VERSION,
      author='Rens Oliemans',
      author_email='hallo+python@rensoliemans.nl',
      install_requires=['covernine'],
      python_requires='>=3.6'
      )
