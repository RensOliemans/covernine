from setuptools import setup

from hardcode.version import VERSION

setup(name='covernine-hardcoded',
      version=VERSION,
      author='Rens Oliemans',
      author_email='hallo+python@rensoliemans.nl',
      install_requires=['covernine'],
      python_requires='>=3.6'
      )
