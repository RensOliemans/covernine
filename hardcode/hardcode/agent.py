import gym

NUM_GAMES = 100000


class NoPossibleMoveException(Exception):
    pass


def determine_action(dice, board):
    moves = valid_moves(dice, board)
    if len(moves) == 0:
        raise NoPossibleMoveException(f'No move possible with dice {dice} and board {board}')
    singles = [m for m in moves if len(m) == 1]
    doubles = [m for m in moves if len(m) == 2]

    if len(singles) > 0:
        return *singles[0], 0, 0

    doubles = sorted(doubles, key=lambda x: min(x) - max(x))
    return *doubles[0], 1


def valid_moves(total, board):
    open_places = [x for x in board if x[1]]
    singles = {(x[0],) for x in open_places if x[0] == total}
    doubles = {tuple(sorted([x[0], y[0]]))
               for x in open_places
               for y in open_places
               if x[0] + y[0] == total and x != y}
    return singles.union(doubles)


def game(env, quiet=True):
    state = env.reset()
    dice, board, _ = state
    action = determine_action(dice, board)
    if not quiet: print(f'Threw {dice}, doing {action}')
    observation, reward, done, info = env.step(action)
    while not done:
        dice, board, _ = observation
        action = determine_action(dice, board)
        if not quiet: print(f'Threw {dice}, doing {action}')
        observation, reward, done, info = env.step(action)
    dice, _, score = observation
    if not quiet: print(f'Over, threw {dice}, got {score}\n')
    return score


def run_singles():
    env = gym.make('covernine:covernine-v0')

    scores = [game(env) for _ in range(NUM_GAMES)]
    avg = sum(scores) / NUM_GAMES
    max_score = max(scores)
    amount_maxs = len([s for s in scores if s == max_score])
    min_score = min(scores)
    amount_mins = len([s for s in scores if s == min_score])

    print(f'Run {NUM_GAMES} single games, had an average score of {avg:.2f}')
    print(f'Maximum score was {max_score}, for {amount_maxs} ({amount_maxs / NUM_GAMES:.2%}) games.\n'
          f'Minimum score was {min_score}, for {amount_mins} ({amount_mins / NUM_GAMES:.2%}) games.')


def run_complete():
    env = gym.make('covernine:covernine-v0')
    total_avg = 0
    max_score = 0
    min_score = 45
    avgs = []

    for e in range(NUM_GAMES):
        score = 0
        count = 0
        while score < 100:
            score += game(env)
            count += 1
        avg = score / count
        total_avg += avg
        avgs.append(avg)
        max_score = max(max_score, avg)
        min_score = min(min_score, avg)
        # print(f'Played until 100, took {count} rounds. Average of {avg:.2f} points.')
    total_avg /= NUM_GAMES
    print(f'Over {NUM_GAMES} games, had an average score of {total_avg:.2f}')
    print(f'Maximum score was {max_score:.2f}, minimum was {min_score:.2f}. Average between them is {(max_score + min_score) / 2:.2f}')

    with open('out.csv', 'w+') as f:
        for avg in avgs:
            f.write(f'{avg}\n')

    print(f'Over {NUM_GAMES} got an average of {total_avg:.2f} per game')


if __name__ == '__main__':
    run_singles()
