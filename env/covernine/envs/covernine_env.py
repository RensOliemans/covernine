import random

import numpy as np
import gym
from gym.spaces import Discrete, Tuple, Box

from covernine.envs.util import compute_valid_moves


def throw():
    return random.randrange(1, 7), random.randrange(1, 7)


class CoverNineEnv(gym.Env):
    def __init__(self):
        super(CoverNineEnv, self).__init__()
        self.dice = 1, 1
        self._reset_board()

        self.observation_space = Box(low=-10, high=45, shape=(11,))

        self.action_space = Tuple((Discrete(9), Discrete(9)))

    def seed(self, seed=None):
        random.seed(seed)

    def step(self, action):
        if sum(action) != sum(self.dice) or action not in self.valid_moves:
            return self.observation, -100, False, {}

        for m in action:
            self.board[m - 1] = False

        self.dice = throw()
        if len(self.valid_moves) == 0:
            return self.observation, -self.score, True, {}

        return self.observation, 0, False, {}

    def reset(self):
        self._reset_board()
        return self.observation

    def render(self, mode='human'):
        print(self.observation)

    def _reset_board(self):
        self.dice = throw()
        self.board = np.array([True for _ in range(1, 10)])

    @property
    def score(self):
        if not any(self.board):
            return -10
        return sum(i+1 for i, x in enumerate(self.board) if x)

    @property
    def observation(self):
        return np.array([sum(self.dice), *map(np.int, self.board), self.score])

    @property
    def valid_moves(self):
        return compute_valid_moves(sum(self.dice), self.board)
