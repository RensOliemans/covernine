def compute_valid_moves(total, board):
    open_places = [x for x in board if x]
    return {tuple(sorted([i, j]))
            for i, x in enumerate(open_places)
            for j, y in enumerate(open_places)
            if i + j == total and i != j}
