from gym.envs.registration import register
from covernine.envs import util

register(
    id='covernine-v0',
    entry_point='covernine.envs:CoverNineEnv'
)
