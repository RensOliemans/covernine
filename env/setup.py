from setuptools import setup

from covernine.version import VERSION

setup(name='covernine',
      version=VERSION,
      author='Rens Oliemans',
      author_email='hallo+python@rensoliemans.nl',
      install_requires=['gym'],
      python_requires='>=3.6'
      )
